import 'package:flutter/material.dart';
import 'products.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final List<Product> _products = [];
  @override
  void iniState(){
    _products.add(Product("assets/images/ninjaaa.jpg","ninja may cry",200));
    _products.add(Product("assets/images/ninja.jpg","ninjaaa may cry",190));
    _products.add(Product("assets/images/ninjaaa.jpg","ninja may cry",200));
    _products.add(Product("assets/images/ninja.jpg","ninjaaa may cry",190));

    super.initState()
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("G-store ESPRIT"),
      ),
      body: GridView.builder(
        itemCount: _products.length,
          itemBuilder:(BuildContext context,int index){
          return Product(_products[index].image, _products[index].tiitle, _products[index]._priice)
          }
          gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent
      ),

      )
    );
  }
}
