import 'package:flutter/material.dart';

class Products extends StatelessWidget {
  final String _image;
  final String title;
  final int _price;


  Products(this._image, this.title, this._price);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
              margin: EdgeInsets.fromLTRB(10, 10, 20, 10),
              child: Image.asset(_image, width: 150,height: 120,)
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(title),
              SizedBox(
                height: 10,
              ),
              Text(_price.toString()+"TND",textScaleFactor: 2,),
           ],
          ),
        ],
      ),
    );
  }
}
class Product {
  final String _immage;
  final String tiitle;
  final int _priice;
  Product(this._immage, this.tiitle, this._priice);

  @override
  String toString() {
    return 'Product{_immage: $_immage, tiitle: $tiitle, _priice: $_priice}';
  }
}
